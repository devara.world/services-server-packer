# pub-a/docker
path "kv-v1/prod/pub-a/docker/*" {
  capabilities = [ "read" ]
}

# pub-b/docker
path "kv-v1/prod/pub-b/docker/*" {
  capabilities = [ "read" ]
}
