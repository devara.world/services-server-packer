{
  "datacenter": "@NET_NAME@",
  "node_name": "@NET_NAME@_consul-slave",
  "server": true,
  "log_level": "DEBUG",
  "enable_syslog": false,
  "enable_debug": true,
  "ui_config": {
    "enabled": true
  },
  "data_dir": "/consul/data",
  "addresses": {
    "https": "0.0.0.0"
  },
  "ports": {
    "http": -1,
    "https": 8501
  },
  "retry_join": [
    "consul-server",
    "consul-client"
  ],
  "encrypt": "@CONSUL_GOSSIP_ENCRYPTION_KEY@",
  "connect": {
    "enabled": true
  },
  "leave_on_terminate": true,
  "skip_leave_on_interrupt": true,
  "rejoin_after_leave": true,
  "enable_local_script_checks": true,
  "verify_incoming": false,
  "verify_outgoing": true,
  "verify_server_hostname": true,
  "ca_file": "/consul/data/x509/consul-agent-ca.pem",
  "cert_file": "/consul/data/x509/@NET_NAME@-server-consul-0.pem",
  "key_file": "/consul/data/x509/@NET_NAME@-server-consul-0-key.pem",
  "auto_encrypt": {
    "allow_tls": true
  },
  "acl":  {
    "enabled": true,
    "default_policy": "deny",
    "enable_token_persistence": true
  },
  "services": [
    {
      "id": "@NET_NAME@_consul-slave_dns",
      "name": "@NET_NAME@_consul-slave_dns",
      "tags": [
        "primary"
      ],
      "address": "127.0.0.1",
      "port": 8600,
      "checks": [
        {
          "id": "dns",
          "name": "Local Consul DNS UDP",
          "tcp": "localhost:8600",
          "interval": "10s",
          "timeout": "1s",
          "success_before_passing": 3,
          "failures_before_warning": 1,
          "failures_before_critical": 3
        }
      ]
    }
  ]
}
