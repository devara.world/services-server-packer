source "hcloud" "base" {
  image_filter {
    with_selector = ["name==base", "stage==2", "final==yes"]
    most_recent   = true
  }
  location     = var.server_location
  server_type  = var.server_type
  ssh_username = "root"
  snapshot_labels = {
    name  = "admin"
    final = "yes"
    tag = "latest"
    repo = "services-server-packer"
    net = var.net_name
  }
  snapshot_name = var.snapshot_name
  token         = var.hcloud_token

}

