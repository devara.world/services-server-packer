#!/bin/bash

set -e
set -u 

S_FILE=$( readlink -f $0 )
S_DIR="${S_FILE%/*}"
TEMP_DIR=$( readlink -f ${S_DIR}/../temp )

which packer 2>/dev/null || {

if [ ! -f ${TEMP_DIR}/packer ]; then
    mkdir  ${TEMP_DIR}
    cd ${TEMP_DIR}
    curl -L https://releases.hashicorp.com/packer/1.8.3/packer_1.8.3_linux_amd64.zip --output packer.zip
    unzip packer.zip
    cd -
fi

}



sed \
  -e "s/@HETZNER_CLOUD_TOKEN@/${HETZNER_CLOUD_TOKEN}/g" \
  -e "s/@GITLAB_GIT_PULL@/${GITLAB_GIT_PULL}/g" \
  -e "s/@CI_REGISTRY_USER@/${CI_REGISTRY_USER}/g" \
  -e "s/@CI_REGISTRY_PASSWORD@/${CI_REGISTRY_PASSWORD}/g" \
  -e "s/@CI_REGISTRY@/${CI_REGISTRY}/g" \
  -e "s#@MGMT_A_SSH_KEY@#${MGMT_A_SSH_KEY}#g" \
  -e "s#@MGMT_B_SSH_KEY@#${MGMT_B_SSH_KEY}#g" \
  main.pkr.hcl.tpl > main.pkr.hcl
