#!/bin/bash

set -e
set -u 

cat <<EOF > creds.auto.tfvars
namecheap_user  = "${NAMECHEAP_USER}"
namecheap_token = "${NAMECHEAP_TOKEN}"
hetzner_dns_token = "${HETZNER_DNS_TOKEN}"
hetzner_cloud_token = "${HETZNER_CLOUD_TOKEN}"
hetzner_ssh_key = "${HETZNER_SSH_KEY}"
EOF

