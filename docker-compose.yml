version: '3.8'

services:
  traefik:
    image: traefik
    container_name: traefik
    restart: 'always'
    command: >
      --configfile=/config/traefik.yaml
    labels:
      com.centurylinklabs.watchtower.enable: true
      traefik.enable: false
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./traefik-config:/config
      - ./letsencrypt:/letsencrypt
    ports:
      - 80:80
      - 443:443
      - 3000:8080
    networks:
      - traefik-network

  health:
    image: registry.gitlab.com/devara.world/health-app:latest
    container_name: health-app
    restart: 'always'
    labels:
      com.centurylinklabs.watchtower.enable: true
      traefik.enable: true
      traefik.http.routers.health.rule: Host("health.${DNS_ZONE}")
      traefik.http.routers.health.entrypoints: websecure
      traefik.http.routers.health.tls.certresolver: staging-resolver
      traefik.http.services.health.loadbalancer.server.port: 3000
      traefik.docker.network: services-server-packer_traefik-network
    expose:
      - 3000
    networks:
      - traefik-network
      - consul-network

  consul-server:
    image: hashicorp/consul
    container_name: consul-server
    restart: always
    volumes:
      - ./consul/server.json:/consul/config/server.json
      - ${VOLUME_MNT}/consul-server/certs:/consul/config/certs
      - ${VOLUME_MNT}/consul-server/data:/consul/data
    command: "agent -bootstrap-expect=3 -config-file=/consul/config/server.json"
    environment:
      - CONSUL_HTTP_TOKEN=your-token
      - CONSUL_HTTP_ADDR=https://localhost:8501
      - CONSUL_CACERT=/consul/data/x509/consul-agent-ca.pem
    deploy:
      resources:
        limits:
          cpus: '0.50'
          memory: 300M
        reservations:
          memory: 200M
    healthcheck:
      test: ["CMD", "curl", "-kvL", "https://localhost:8501/v1/agent/checks"]
      interval: ${HEALTHCHECK_INTERVAL}
      timeout: ${HEALTHCHECK_TIMEOUT}
      retries: ${HEALTHCHECK_RETRIES}
    ports:
      - "8501:8501"
      - "8600:8600/tcp"
      - "8600:8600/udp"
    networks:
      - consul-network

  consul-slave:
    image: hashicorp/consul
    container_name: consul-slave
    restart: always
    volumes:
      - ./consul/slave.json:/consul/config/slave.json
      - ${VOLUME_MNT}/consul-slave/certs:/consul/config/certs
      - ${VOLUME_MNT}/consul-slave/data:/consul/data
    command: "agent -bootstrap-expect=3 -config-file=/consul/config/slave.json"
    environment:
      - CONSUL_HTTP_TOKEN=your-token
      - CONSUL_HTTP_ADDR=https://localhost:8501
      - CONSUL_CACERT=/consul/data/x509/consul-agent-ca.pem
    deploy:
      resources:
        limits:
          cpus: '0.50'
          memory: 300M
        reservations:
          memory: 200M
    healthcheck:
      test: ["CMD", "curl", "-kvL", "https://localhost:8501/v1/agent/checks"]
      interval: ${HEALTHCHECK_INTERVAL}
      timeout: ${HEALTHCHECK_TIMEOUT}
      retries: ${HEALTHCHECK_RETRIES}
    ports:
      - "8511:8501"
      - "8610:8600/tcp"
      - "8610:8600/udp"
    networks:
      - consul-network


  consul-client:
    image: hashicorp/consul
    container_name: consul-client
    restart: always
    volumes:
      - ./consul/client.json:/consul/config/client.json
      - ${VOLUME_MNT}/consul-client/certs:/consul/config/certs
      - ${VOLUME_MNT}/consul-client/data:/consul/data
    command: "agent -bootstrap-expect=3 -config-file=/consul/config/client.json"
    environment:
      - CONSUL_HTTP_TOKEN=your-token
      - CONSUL_HTTP_ADDR=https://localhost:8501
      - CONSUL_CACERT=/consul/data/x509/consul-agent-ca.pem
    deploy:
      resources:
        limits:
          cpus: '0.50'
          memory: 300M
        reservations:
          memory: 200M
    healthcheck:
      test: ["CMD", "curl", "-kvL", "https://localhost:8501/v1/agent/checks"]
      interval: ${HEALTHCHECK_INTERVAL}
      timeout: ${HEALTHCHECK_TIMEOUT}
      retries: ${HEALTHCHECK_RETRIES}
    ports:
      - "8521:8501"
      - "8620:8600/tcp"
      - "8620:8600/udp"
    networks:
      - consul-network

  vault:
    image: hashicorp/vault
    container_name: vault
    restart: always
    volumes:
      - ${VOLUME_MNT}/vault/file:/vault/file:rw
      - ./vault:/vault/config:rw
    entrypoint: vault server -config=/vault/config/config.hcl
    cap_add:
      - IPC_LOCK
    environment:
      - VAULT_ADDR=http://0.0.0.0:8200
      - VAULT_API_ADDR=http://0.0.0.0:8200
      - VAULT_ADDRESS=http://0.0.0.0:8200
      - VAULT_CLUSTER_ADDR=http://0.0.0.0:8201
    deploy:
      resources:
        limits:
          cpus: '0.50'
          memory: 200M
        reservations:
          memory: 100M
    healthcheck:
      test: ["CMD", "vault", "pki", "health-check", "-list", "pki-root/"]
      interval: ${HEALTHCHECK_INTERVAL}
      timeout: ${HEALTHCHECK_TIMEOUT}
      retries: ${HEALTHCHECK_RETRIES}
    ports:
      - 8200:8200
      - 8201:8201
    networks:
      - vault-network

networks:
  traefik-network:
  vault-network:
  consul-network:

