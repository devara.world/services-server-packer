# Build Packer command
# $ packer build .

build {
  sources = ["source.hcloud.base"]
  name    = "base"

  provisioner "shell" {
    environment_vars = [
      "GITLAB_GIT_PULL=@GITLAB_GIT_PULL@",
      "CI_REGISTRY_USER=@CI_REGISTRY_USER@",
      "CI_REGISTRY_PASSWORD=@CI_REGISTRY_PASSWORD@",
      "CI_REGISTRY=@CI_REGISTRY@",
      "MGMT_A_SSH_KEY=@MGMT_A_SSH_KEY@",
      "MGMT_B_SSH_KEY=@MGMT_B_SSH_KEY@",
      "NET_NAME=${var.net_name}",
      "VOLUME_MNT=${var.volume_mnt}"
    ]
    script       = "install.sh"
    pause_before = "1s"
    timeout      = "300s"
  }

}
