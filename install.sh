#!/bin/bash


set -euo pipefail
set -x


#
# *** install software ***
#
export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=critical
apt-get -qy update
apt-get -qy upgrade
apt-get -qy autoclean

pip3 install -U ansible docker-compose



#
# *** change ssh config ***
# * disable root login
# * change ssh port
#
sed --in-place -e 's/^PermitRootLogin.*/PermitRootLogin no/g' \
  -e 's/^#Port.*/Port 1911/g' \
  /etc/ssh/sshd_config
systemctl restart sshd

cat /etc/ssh/sshd_config



#
# create workload
#
sudo -u workload --preserve-env=CI_JOB_TOKEN --preserve-env=CI_REGISTRY_USER --preserve-env=CI_REGISTRY_PASSWORD --preserve-env=CI_REGISTRY --preserve-env=NET_NAME --preserve-env=VOLUME_MNT -s <<'AS_USER'
echo "Hi from $USER."
cd /home/workload
git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/devara.world/services-server-packer.git
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
cd services-server-packer

CONSUL_GOSSIP_ENCRYPTION_KEY=$( docker run hashicorp/consul:1.15 consul keygen )
echo $CONSUL_GOSSIP_ENCRYPTION_KEY
sed \
  -e "s/@NET_NAME@/${NET_NAME}/g" \
  -e "s#@VOLUME_MNT@#${VOLUME_MNT}#g" \
  -e "s#@CONSUL_GOSSIP_ENCRYPTION_KEY@#${CONSUL_GOSSIP_ENCRYPTION_KEY}#g" \
  docker-compose.env.tpl > .env
cat .env
cd consul
sed \
  -e "s/@NET_NAME@/${NET_NAME}/g" \
  -e "s#@CONSUL_GOSSIP_ENCRYPTION_KEY@#${CONSUL_GOSSIP_ENCRYPTION_KEY}#g" \
  server.json.tpl > server.json
sed \
  -e "s/@NET_NAME@/${NET_NAME}/g" \
  -e "s#@CONSUL_GOSSIP_ENCRYPTION_KEY@#${CONSUL_GOSSIP_ENCRYPTION_KEY}#g" \
  slave.json.tpl > slave.json
sed \
  -e "s/@NET_NAME@/${NET_NAME}/g" \
  -e "s#@CONSUL_GOSSIP_ENCRYPTION_KEY@#${CONSUL_GOSSIP_ENCRYPTION_KEY}#g" \
  client.json.tpl > client.json
cd -

docker-compose pull 
AS_USER

cp /home/workload/services-server-packer/docker-apps.service /etc/systemd/system/docker-apps.service
systemctl enable docker-apps
ls -la /etc/systemd/system/docker-apps.service
ls -la /etc/systemd/system/multi-user.target.wants/docker-apps.service


echo -e "\nSUCCESSFUL!!"
