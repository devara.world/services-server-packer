DEPL         ?= dev
VAR_FILE     ?= $(DEPL).tfvars
CREDS_FILE   ?= creds.tfvars

export PATH := ./temp:$(PATH)


help:                     ## printing out the help
	@echo
	@echo packer deployment
	@echo
	@echo --- TARGETS ---
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

fmt:                      ## format packer files
	packer fmt -recursive .

validate:                 ## validate packer files
	packer validate source.pkr.hcl

build: validate           ## apply terroform deployment
	packer build -force -var=hcloud_token=${HETZNER_CLOUD_TOKEN} -var=snapshot_name=services-server-latest -var=net_name=services-a -var=volume_mnt=/mnt/HC_Volume_30026381 .
	packer build -force -var=hcloud_token=${HETZNER_CLOUD_TOKEN} -var=snapshot_name=services-server-latest -var=net_name=services-b -var=volume_mnt=/mnt/HC_Volume_30026381 .

init:                     ## initialize packer
	./scripts/init.sh
